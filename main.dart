import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:module3/splashscreen.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      // ignore: prefer_const_constructors
      options: FirebaseOptions(
          apiKey: "AIzaSyC3rvnCdFSXoYwtVRWUuQWPu2z7Nd-npl4",
          authDomain: "siphephelo-nene-module5.firebaseapp.com",
          projectId: "siphephelo-nene-module5",
          storageBucket: "siphephelo-nene-module5.appspot.com",
          messagingSenderId: "1026504371808",
          appId: "1:1026504371808:web:207ea52ce48b944c755d05"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "AgriSmart App",
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const SplashScreen(),
    );
  }
}
